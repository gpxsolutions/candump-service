#!/bin/sh

set -e

mkdir -p /var/log/candump
mkdir -p /etc/systemd/system/logrotate.service.d
mkdir -p /etc/systemd/system/logrotate.timer.d
install --mode=u=rw,go=r candump.service            /etc/systemd/system/
install --mode=u=rw,go=r candump.logrotate          /etc/logrotate.d/candump
install --mode=u=rw,go=r logrotate.service.override /etc/systemd/system/logrotate.service.d/override.conf
install --mode=u=rw,go=r logrotate.timer.override   /etc/systemd/system/logrotate.timer.d/override.conf
systemctl daemon-reload
systemctl enable candump.service --now --no-block
